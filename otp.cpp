#include <iostream>
#include <fstream>
#include <vector>

void printVerbose()
{
    std::cout << "OTP v1.0" << std::endl;
    std::cout << "This is a simple one time pad encryption program." << std::endl;
    std::cout << "It will use the one time pad mode of operation to" << std::endl;
    std::cout << "encrypt an input file against a keyfile. The input" << std::endl;
    std::cout << "file size must be smaller than the keyfile size to" << std::endl;
    std::cout << "ensure proper encryption strength is employed." << std::endl;
    std::cout << std::endl;
    std::cout << "Usage: otp -i <input file> -k <key file> -o <output file>" << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 7) {
        printVerbose();
        return 0;
    }

    std::vector<std::string> commandLine;
    for (int i = 0; i < argc; i++) {
        commandLine.push_back(std::string(argv[i]));
    }

    std::ifstream inputFile;
    std::ifstream keyFile;
    std::ofstream outputFile;

    for (int i = 0; i < commandLine.size(); i++) {
        std::string argument = commandLine[i];
        if (argument == "-i" || argument == "--input-file") {
            inputFile.open(commandLine[i + 1], std::ios::in | std::ios::binary);
        }

        if (argument == "-k" || argument == "--key-file") {
            keyFile.open(commandLine[i + 1], std::ios::in | std::ios::binary);
        }

        if (argument == "-o" || argument == "--output-file") {
            outputFile.open(commandLine[i + 1], std::ios::out | std::ios::binary);
        }

        if (argument == "-h" || argument == "--help") {
            printVerbose();
            return 0;
        }
    }

    if (!inputFile.is_open()) {
        std::cerr << "Error: Input File failed to open" << std::endl;
        return -1;
    }

    if (!keyFile.is_open()) {
        std::cerr << "Error: Key File failed to open" << std::endl;
        return -1;
    }

    if (!outputFile.is_open()) {
        std::cerr << "Error: Output File failed to open" << std::endl;
        return -1;
    }

    inputFile.seekg(0, std::ios::end);
    std::streampos inputFileLength = inputFile.tellg();
    inputFile.seekg(0, std::ios::beg);

    keyFile.seekg(0, std::ios::end);
    std::streampos keyFileLength = keyFile.tellg();
    keyFile.seekg(0, std::ios::beg);

    if (inputFileLength > keyFileLength) {
        std::cerr << "Error: The input file size is larger than the keyfile size." << std::endl;
        std::cerr << "Error: This results in insufficent cryptographic strength." << std::endl;
        std::cerr << "Terminating Program." << std::endl;
        return -2;
    }

    char key = 0x00;
    char plainText = 0x00;
    char cipherText = 0x00;

    while (!inputFile.eof()) {
        keyFile.get(key);
        inputFile.get(plainText);
        cipherText = (plainText ^ key);
        outputFile.put(cipherText);
    }

    inputFile.close();
    keyFile.close();
    outputFile.close();

    return 0;
}
